package com.example.flickrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Vector;

import static java.security.AccessController.getContext;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        ListView listView = (ListView)findViewById(R.id.list);
        MyAdapter adapter = new MyAdapter();
        listView.setAdapter(adapter);
        MainActivity.AsyncFlickrJSONDataForList asyncList = new MainActivity.AsyncFlickrJSONDataForList(adapter);
        asyncList.execute("https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json");
    }
    public class MyAdapter extends BaseAdapter {

        Vector<String> vector = new Vector<>(10);

        public void dd(String url) {
                vector.add(url);
                notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            if (vector == null) {
                return 1;
            }
            else {
                return vector.size();
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getApplicationContext())
                        .inflate(R.layout.textviewlayout,null);
                TextView tv = (TextView) convertView.findViewById(R.id.textView);
                tv.setText(vector.get(position));
            }

            TextView tv = (TextView) convertView.findViewById(R.id.textView);
            tv.setText(vector.get(position));
            Log.i("test", vector.get(position) + String.valueOf(position));
            return convertView;
        }

        @Override
        public String getItem(int position) {
            return vector.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }
    }
}