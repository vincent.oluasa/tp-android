package com.example.flickrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Button bImage = (Button)findViewById(R.id.getanimage);
        bImage.setOnClickListener(new GetImageOnClickListener() {
            @Override
            public void onClick(View v) {
                super.onClick(v);
            }
        });
        Button bList = (Button)findViewById(R.id.toList);
        bList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent listActivity = new Intent(getApplicationContext(), ListActivity.class);
                startActivity(listActivity);
            }
        });
    }

    public class GetImageOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            AsyncFlickrJSONData imagesData = new AsyncFlickrJSONData();
            imagesData.execute("https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json");
        }
    }

    public class AsyncFlickrJSONData extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... strings) {
            String flickrUrl = strings[0];
            JSONObject jsonFlickr = null;
            URL url = null;
            try {
                url = new URL(flickrUrl);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String s1 = readStream(in);
                    int lengthS = s1.length();
                    String s = (String) s1.subSequence(15, lengthS-1);
                    jsonFlickr = new JSONObject(s);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return jsonFlickr;
        }

        @Override
        protected void onPostExecute(JSONObject jsonFlickr) {
            super.onPostExecute(jsonFlickr);
            try {
                String firstUrl = jsonFlickr.getJSONArray("items").getJSONObject(0).getString("link");
                AsyncBitmapDownloader firstAsyncImage = new AsyncBitmapDownloader();
                firstAsyncImage.execute(firstUrl);
                Log.i("JFL", firstUrl);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private String readStream(InputStream in) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder sb = new StringBuilder();

            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
                Log.e(TAG, "IOException", e);
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    Log.e(TAG, "IOException", e);
                }
            }
            return sb.toString();
        }
    }

    public class AsyncBitmapDownloader extends AsyncTask<String, Void, Bitmap> {

        ImageView firstImage = (ImageView) findViewById(R.id.image);

        @Override
        protected Bitmap doInBackground(String... strings) {
            String imageUrl = strings[0];
            Bitmap bm = null;
            URL url = null;
            try {
                url = new URL(imageUrl);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    bm = BitmapFactory.decodeStream(in);
                } finally {
                    urlConnection.disconnect();
                }
            } catch(IOException e){
                    e.printStackTrace();
            }
            return bm;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            firstImage.setImageBitmap(bitmap);
        }
    }

    public static class AsyncFlickrJSONDataForList extends AsyncTask<String, Void, JSONObject> {

        private final WeakReference<ListActivity.MyAdapter> adapterReference;

        public AsyncFlickrJSONDataForList(ListActivity.MyAdapter adapter) {
            adapterReference = new WeakReference<ListActivity.MyAdapter>(adapter);
        }

        @Override
        protected JSONObject doInBackground(String... strings) {
            String flickrUrl = strings[0];
            JSONObject jsonFlickr = null;
            URL url = null;
            try {
                url = new URL(flickrUrl);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String s1 = readStream(in);
                    int lengthS = s1.length();
                    String s = (String) s1.subSequence(15, lengthS-1);
                    jsonFlickr = new JSONObject(s);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return jsonFlickr;
        }

        @Override
        protected void onPostExecute(JSONObject jsonFlickr) {
            super.onPostExecute(jsonFlickr);
            if (adapterReference != null) {
                ListActivity.MyAdapter adapter = adapterReference.get();
                try {
                    JSONArray urlArray = jsonFlickr.getJSONArray("items");
                    for (int i = 0; i < urlArray.length(); i++) {
                        String url = urlArray.getJSONObject(i).getString("link");
                        adapter.dd(url);
                        Log.i("JFL", "Adding to adapter url : " + url);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        private String readStream(InputStream in) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder sb = new StringBuilder();

            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
                Log.e(TAG, "IOException", e);
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    Log.e(TAG, "IOException", e);
                }
            }
            return sb.toString();
        }
    }
}

